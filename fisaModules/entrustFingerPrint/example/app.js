// This is a test harness for your module
// You should do something interesting in this harness
// to test out the module and to provide instructions
// to users on how to use it by example.


// open a single window
var win = Ti.UI.createWindow({
	backgroundColor:'white'
});
var label = Ti.UI.createLabel();
win.add(label);
win.open();

// TODO: write your module tests here
var entrustfingerprint = require('com.fisa.entrust.deviceFingerPrint');
Ti.API.info("module is => " + entrustfingerprint);

label.text = entrustfingerprint.example();

Ti.API.info("module exampleProp is => " + entrustfingerprint.exampleProp);
entrustfingerprint.exampleProp = "This is a test value";

if (Ti.Platform.name == "android") {
	var proxy = entrustfingerprint.createExample({
		message: "Creating an example Proxy",
		backgroundColor: "red",
		width: 100,
		height: 100,
		top: 100,
		left: 150
	});

	proxy.printMessage("Hello world!");
	proxy.message = "Hi world!.  It's me again.";
	proxy.printMessage("Hello world!");
	win.add(proxy);
}

