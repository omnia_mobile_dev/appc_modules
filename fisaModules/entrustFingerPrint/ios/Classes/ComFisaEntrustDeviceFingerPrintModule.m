/**
 * entrustFingerPrint
 *
 * Created by Your Name
 * Copyright (c) 2018 Your Company. All rights reserved.
 */

#import "ComFisaEntrustDeviceFingerPrintModule.h"
#import "TiBase.h"
#import "TiHost.h"
#import "TiUtils.h"
#import "ETDFDeviceFingerprint.h"

@implementation ComFisaEntrustDeviceFingerPrintModule

#pragma mark Internal

// This is generated for your module, please do not change it
- (id)moduleGUID
{
  return @"b217fb97-2107-4e78-88ed-b5b57669eb54";
}

// This is generated for your module, please do not change it
- (NSString *)moduleId
{
  return @"com.fisa.entrust.deviceFingerPrint";
}

#pragma mark Lifecycle

- (void)startup
{
  // This method is called when the module is first loaded
  // You *must* call the superclass
  [super startup];
  DebugLog(@"[DEBUG] %@ loaded", self);
}

#pragma Public APIs

- (NSString *)example:(id)args
{
  // Example method. 
  // Call with "MyModule.example(args)"
  return @"hello world";
}

- (NSString *)getDeviceId:(id)args
{
    ETDFDeviceFingerprint *fp = [[ETDFDeviceFingerprint alloc] init];
    NSString *deviceId = [fp generateDeviceId];
    return deviceId;
}

- (NSString *)exampleProp
{
  // Example property getter. 
  // Call with "MyModule.exampleProp" or "MyModule.getExampleProp()"
  return @"Titanium rocks!";
}

- (void)setExampleProp:(id)value
{
  // Example property setter. 
  // Call with "MyModule.exampleProp = 'newValue'" or "MyModule.setExampleProp('newValue')"
}

@end
