//
//  ETDFAttributeLanguages.h
//  Entrust IdentityGuard Device Fingerprint SDK
//
//  Copyright (c) 2015 Entrust Inc. All rights reserved.
//  Use is subject to the terms of the accompanying license agreement. Entrust Confidential.
//

#import <Foundation/Foundation.h>
#import "ETDFDeviceAttribute.h"

/**
 * Collects the list of languages configured for this device.
 */
@interface ETDFAttributeLanguages : NSObject<ETDFDeviceAttribute>

- (NSString *) name;
- (id) value;

@end
