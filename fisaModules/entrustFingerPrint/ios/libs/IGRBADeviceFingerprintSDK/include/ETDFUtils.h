//
//  ETDFUtils.h
//  Entrust IdentityGuard Device Fingerprint SDK
//
//  Copyright (c) 2015 Entrust Inc. All rights reserved.
//  Use is subject to the terms of the accompanying license agreement. Entrust Confidential.
//

#import <Foundation/Foundation.h>

/**
 * Utility class containing helper methods.
 */
@interface ETDFUtils : NSObject

/**
 * Converts the given NSData object to a hex-encoded string.
 * @param data The data to convert.
 * @return The data as a hex encoded string.
 */
+ (NSString *)hexStringFromData:(NSData *)data;

/**
 * Converts the given NSTimeZone object to a string
 * in the format of UTC+01 or UTC-03:30.
 * Note UTC time is represented as UTC+00
 * @param timezone The input timezone.
 * @return The string representation of the timezone.
 */
+ (NSString *)timezoneToUTCOffset:(NSTimeZone *)timezone;

/**
 * Calculates a SHA-256 hash on the given data.
 * @param data The input data to hash.
 * @return The hashed data.
 */
+ (NSData *)sha256:(NSData *)data;

/**
 * Returns the OS major version number (e.g. 7 or 8).
 * @return The OS major version number.
 */
+ (NSUInteger)deviceSystemMajorVersion;

@end
