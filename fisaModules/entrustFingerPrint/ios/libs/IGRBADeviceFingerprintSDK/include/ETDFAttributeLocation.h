//
//  ETDFAttributeLocation.h
//  Entrust IdentityGuard Device Fingerprint SDK
//
//  Copyright (c) 2015 Entrust Inc. All rights reserved.
//  Use is subject to the terms of the accompanying license agreement. Entrust Confidential.
//

#import <Foundation/Foundation.h>
#import "ETDFDeviceAttribute.h"
#import <CoreLocation/CoreLocation.h>

typedef void (^ETDFLocationCollectionCompletionBlock)(BOOL success, NSError *error);

/**
 * Collects the current device location.  This requires special permission
 * to access the user location and is collected asynchronously due to the
 * nature of invoking the GPS capabilities of the device.
 */
@interface ETDFAttributeLocation : NSObject<ETDFDeviceAttribute, CLLocationManagerDelegate>

- (NSString *) name;
- (id) value;
- (void)collectLocationIgnoreLocationNotEnabled:(BOOL)ignoreLocationNotEnabled withCallback:(ETDFLocationCollectionCompletionBlock)callback;

@end
