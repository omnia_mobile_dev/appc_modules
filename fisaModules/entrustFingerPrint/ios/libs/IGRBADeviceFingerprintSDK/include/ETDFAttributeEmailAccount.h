//
//  ETDFAttributeEmailAccount.h
//  Entrust IdentityGuard Device Fingerprint SDK
//
//  Copyright (c) 2015 Entrust Inc. All rights reserved.
//  Use is subject to the terms of the accompanying license agreement. Entrust Confidential.
//

#import <Foundation/Foundation.h>
#import "ETDFDeviceAttribute.h"

/**
 * Collects whether the user has an email account configured
 * on this device.
 */
@interface ETDFAttributeEmailAccount : NSObject<ETDFDeviceAttribute>

- (NSString *) name;
- (id) value;

@end
