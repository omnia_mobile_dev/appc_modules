//
//  ETDFAttributeDeviceName.h
//  Entrust IdentityGuard Device Fingerprint SDK
//
//  Copyright (c) 2015 Entrust Inc. All rights reserved.
//  Use is subject to the terms of the accompanying license agreement. Entrust Confidential.
//

#import <Foundation/Foundation.h>
#import "ETDFDeviceAttribute.h"

/**
 * Collects the user defined friendly device name. For instance:
 * John's iPhone 5.
 */
@interface ETDFAttributeDeviceName : NSObject<ETDFDeviceAttribute>

- (NSString *) name;
- (id) value;

@end
