//
//  ETDFAttributeLocale.h
//  Entrust IdentityGuard Device Fingerprint SDK
//
//  Copyright (c) 2015 Entrust Inc. All rights reserved.
//  Use is subject to the terms of the accompanying license agreement. Entrust Confidential.
//

#import <Foundation/Foundation.h>
#import "ETDFDeviceAttribute.h"

/**
 * Collects the current device locale configured by the user.
 */
@interface ETDFAttributeLocale : NSObject<ETDFDeviceAttribute>

- (NSString *) name;
- (id) value;

@end
