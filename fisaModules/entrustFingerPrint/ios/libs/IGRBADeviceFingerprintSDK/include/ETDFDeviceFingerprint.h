//
//  ETDFDeviceFingerprint.h
//  Entrust IdentityGuard Device Fingerprint SDK
//
//  Copyright (c) 2015 Entrust Inc. All rights reserved.
//  Use is subject to the terms of the accompanying license agreement. Entrust Confidential.
//

/*! \mainpage Entrust IdentityGuard Mobile Device Fingerprint SDK
 *
 * \section intro_sec Introduction
 *
 * This is the class documentation for the Entrust IdentityGuard
 * Device Fingerprint SDK.  Select the Classes tab for a brief
 * overview of all the classes in the SDK.  Select the Files tab,
 * followed by File Members to see the list of Typedefs, Enumerations,
 * and Defines.
 *
 * \subsection documentation_sec Documentation
 * In addition to this code generated documentation, there is a
 * Readme_iOS.html file included with the SDK includes
 * instructions on how to link the SDK to your application.
 *
 * The Programmer's Guide contains information on the various use
 * cases supported by the SDK and code samples on how to use them.
 *
 * \subsection example_sec Example Application
 *
 * The SDK includes the source code of an example iPhone application that
 * can be run in the simulator or on a device.  This example application
 * demonstrates how to create device fingerprints.  It is recommended 
 * that you review the example application before proceeding with your 
 * own application.
 *
 * \subsection deviceid_example_sec Generate Device ID
 *
 * To create a device identifier using the standard device identifier attributes,
 * initialize a ETDFDeviceFingerprint object and call
 * {@link ETDFDeviceFingerprint::generateDeviceId}.  For example:<br/>
 * ETDFDeviceFingerprint *fp = [[ETDFDeviceFingerprint alloc] init];<br/>
 * NSString *deviceId = [fp generateDeviceId];<br/>
 * // deviceId will be a 64 hex character identifier.
 *
 * \subsection devicedata_example_sec Generate Device Data
 *
 * To create device data using the standard device data attributes,
 * initialize a ETDFDeviceFingerprint object and call
 * {@link ETDFDeviceFingerprint::generateDeviceData}.  For example:<br/>
 * ETDFDeviceFingerprint *fp = [[ETDFDeviceFingerprint alloc] init];<br/>
 * NSString *deviceData = [fp generateDeviceData];<br/>
 * // deviceData will be a JSON string representing the device data.
 *
 */

#import <Foundation/Foundation.h>

/**
 * The main fingerprinting class which takes an array of attributes
 * and generates a device ID or device data JSON string.
 */
@interface ETDFDeviceFingerprint : NSObject

/**
 * Generates a string device identifier using the device ID attributes
 * provided in the constructor.
 * @return The device ID representing the current device.
 */
- (NSString *) generateDeviceId;

/**
 * Collects the device attributes and returns them as a JSON string.
 * @return The collected device attributes.
 */
- (NSString *) generateDeviceData;

@end
