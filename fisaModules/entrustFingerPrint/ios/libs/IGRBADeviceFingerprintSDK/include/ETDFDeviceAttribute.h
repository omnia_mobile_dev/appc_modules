//
//  ETDFAttribute.h
//  Entrust IdentityGuard Device Fingerprint SDK
//
//  Copyright (c) 2015 Entrust Inc. All rights reserved.
//  Use is subject to the terms of the accompanying license agreement. Entrust Confidential.
//

#ifndef EntrustIGDeviceFingerprintSDK_ETDFAttribute_h
#define EntrustIGDeviceFingerprintSDK_ETDFAttribute_h

/**
 * The ETDFDeviceAttribute Protocol defines the necessary methods
 * to implement in order to provide custom attributes in to the
 * collection engine.
 */
@protocol ETDFDeviceAttribute

/**
 * The name of this attribute.
 * @return The name of the attribute.
 */
- (NSString *) name;

/**
 * The value of this attribute.  Returned values must be supported by
 * the NSJSONSerialization API.
 * @return The value of the attribute.
 */
- (id) value;

@end


#endif
