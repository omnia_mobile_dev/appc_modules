//
//  ETDFAttributeTimeZoneOffset.h
//  Entrust IdentityGuard Device Fingerprint SDK
//
//  Copyright (c) 2015 Entrust Inc. All rights reserved.
//  Use is subject to the terms of the accompanying license agreement. Entrust Confidential.
//

#import <Foundation/Foundation.h>
#import "ETDFDeviceAttribute.h"

/**
 * Collects the current device time zone offset in minutes.
 */
@interface ETDFAttributeTimeZoneOffset : NSObject<ETDFDeviceAttribute>

- (NSString *) name;
- (id) value;

@end
