//
//  ETDFAttributeVendorID.h
//  Entrust IdentityGuard Device Fingerprint SDK
//
//  Copyright (c) 2015 Entrust Inc. All rights reserved.
//  Use is subject to the terms of the accompanying license agreement. Entrust Confidential.
//

#import <Foundation/Foundation.h>
#import "ETDFDeviceAttribute.h"

/**
 * Collects the application vendor ID.  This attribute is consistent across
 * all applications by the same vendor.  If you need a device identifier that
 * spans across multiple app vendors, then this attribute should be excluded
 * from the device identifier attribute list.
 */
@interface ETDFAttributeVendorID : NSObject<ETDFDeviceAttribute>

- (NSString *) name;
- (id) value;

@end
