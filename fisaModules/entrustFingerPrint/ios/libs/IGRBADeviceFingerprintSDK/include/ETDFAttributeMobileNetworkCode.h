//
//  ETDFAttributeMobileNetworkCode.h
//  Entrust IdentityGuard Device Fingerprint SDK
//
//  Copyright (c) 2015 Entrust Inc. All rights reserved.
//  Use is subject to the terms of the accompanying license agreement. Entrust Confidential.
//

#import <Foundation/Foundation.h>
#import "ETDFDeviceAttribute.h"

/**
 * Collects the mobile network code from the cellular provider.
 */
@interface ETDFAttributeMobileNetworkCode : NSObject<ETDFDeviceAttribute>

- (NSString *) name;
- (id) value;

@end
