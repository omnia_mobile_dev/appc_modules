//
//  ETDFAttributeDeviceBootTime.h
//  Entrust IdentityGuard Device Fingerprint SDK
//
//  Copyright (c) 2015 Entrust Inc. All rights reserved.
//  Use is subject to the terms of the accompanying license agreement. Entrust Confidential.
//

#import <Foundation/Foundation.h>
#import "ETDFDeviceAttribute.h"

/**
 * Collects the time when the device was last started.  This
 * is a very unique data point but will change whenever the
 * user restarts their device.
 */
@interface ETDFAttributeDeviceBootTime : NSObject<ETDFDeviceAttribute>

- (NSString *) name;
- (id) value;

@end
