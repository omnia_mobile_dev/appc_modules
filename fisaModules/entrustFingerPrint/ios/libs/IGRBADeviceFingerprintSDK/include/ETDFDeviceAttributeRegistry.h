//
//  ETDFDeviceAttributeRegistry.h
//  Entrust IdentityGuard Device Fingerprint SDK
//
//  Copyright (c) 2015 Entrust Inc. All rights reserved.
//  Use is subject to the terms of the accompanying license agreement. Entrust Confidential.
//

#import <Foundation/Foundation.h>
#import "ETDFDeviceAttribute.h"

#import "ETDFAttributeDeviceModel.h"
#import "ETDFAttributeDeviceMachine.h"
#import "ETDFAttributeDeviceName.h"
#import "ETDFAttributeScreenHeight.h"
#import "ETDFAttributeScreenWidth.h"
#import "ETDFAttributeScreenScale.h"
#import "ETDFAttributeOSVersion.h"
#import "ETDFAttributeDiskSize.h"
#import "ETDFAttributeDeviceBootTime.h"
#import "ETDFAttributeTimeZone.h"
#import "ETDFAttributeTimeZoneOffset.h"
#import "ETDFAttributeTimeZoneOffsetString.h"
#import "ETDFAttributeLanguages.h"
#import "ETDFAttributeLocale.h"
#import "ETDFAttributeMobileISOCountryCode.h"
#import "ETDFAttributeMobileCountryCode.h"
#import "ETDFAttributeMobileNetworkCode.h"
#import "ETDFAttributeDeviceUnsecure.h"
#import "ETDFAttributeDeviceUnsecureReason.h"
#import "ETDFAttributeDynamicType.h"
#import "ETDFAttributeSocialMediaAccounts.h"
#import "ETDFAttributeAccessibilityOptions.h"
#import "ETDFAttributeCarrierName.h"
#import "ETDFAttributeCarrierVOIP.h"
#import "ETDFAttributeVendorID.h"
#import "ETDFAttributeInstalledKeyboards.h"
#import "ETDFAttributeConnectionType.h"
#import "ETDFAttributeAppVersion.h"
#import "ETDFAttributeLocation.h"
#import "ETDFAttributeManufacturer.h"
#import "ETDFAttributeEmailAccount.h"
#import "ETDFAttributeIovationBlackbox.h"

/**
 * The ETDFDeviceAttributeRegistry class is a singleton class
 * which manages the attributes to collect when generating a device
 * fingerprint or device identifier.  Custom attributes can be
 * registered through this class.
 */
@interface ETDFDeviceAttributeRegistry : NSObject

/**
 * Singleton accessor method.
 */
+ (ETDFDeviceAttributeRegistry *) sharedInstance;

/**
 * Returns an array of the device attributes
 * to be included in the device fingerprint data. This
 * array should be used when generating fingerprinting data
 * for IdentityGuard.
 * @return An array of id<ETDFDeviceAttribute> objects.
 */
- (NSArray *)deviceAttributesForDeviceData;

/**
 * Returns an array of the device attributes
 * needed to generate a string device identifier.
 * @return An array of id<ETDFDeviceAttribute> objects.
 */
- (NSArray *) deviceAttributesForDeviceID;

/**
 * Returns an array of all the registered device attributes.
 * @return An array of id<ETDFDeviceAttribute> objects.
 */
- (NSArray *)allDeviceAttributes;

/**
 * Register a custom device attribute.  Duplicates will not be
 * registered.
 * @param forDeviceData Whether this attribute will be included
 * in the standard device fingerprint data attributes.
 * @param forDeviceID Whether this attribute will be included
 * in the standard device identifier attributes.
 * @return YES if the attribute was registered. No if the attribute
 * couldn't be registered.
 */
- (BOOL) addCustomDeviceAttribute:(id<ETDFDeviceAttribute>)attribute forDeviceData:(BOOL)forDeviceData forDeviceID:(BOOL)forDeviceID;

/**
 * Retrieve an attribute from the registry with the given name.
 * @param The name of the attribute to retrieve.
 * @return The attribute if it exists, nil otherwise.
 */
- (id<ETDFDeviceAttribute>) deviceAttributeWithName:(NSString *)name;

/**
 * Returns whether or not a device attribute with the given name exists.
 * @param The name of the attribute to check.
 * @return YES if the attribute exists, NO otherwise.
 */
- (BOOL) hasDeviceAttributeWithName:(NSString *)name;

/**
 * Removes a device attribute from the device ID or device data registry.
 * @param name The name of the attribute to remove.
 * @param fromDeviceData Whether to remove it from the device fingerprint data attributes.
 * @param fromDeviceId Whether to remove it from the device ID attributes.
 */
- (void) removeDeviceAttribute:(NSString *)name fromDeviceData:(BOOL)fromDeviceData fromDeviceID:(BOOL)fromDeviceID;

/**
 * Replaces the existing list of device attributes to use when generating the
 * unique device identifier with the provided list of device attributes.
 * @param attributes The new list of device attributes to use when 
 * generating a unique device identifier.
 */
- (void) setDeviceAttributesForDeviceId:(NSArray *)attributes;

/**
 * Replaces the existing list of device attributes to use when generating the
 * list of device data with the provided list of device attributes.
 * @param attributes The new list of device attributes to use when generating 
 * the list of device data points gathered.
 */
- (void) setDeviceAttributesForDeviceData:(NSArray *)attributes;

/**
 * Includes collection of the device's location.  This is an asynchronous collection
 * of the location.  If using this attribute make sure to include the NSLocationWhenInUseUsageDescription
 * entry in your Info.plist file to describe why your application needs location
 * services.  This is required on iOS 8 and newer.
 * @param forDeviceData Whether this attribute will be included
 * in the standard device fingerprint data attributes.
 * @param forDeviceID Whether this attribute will be included
 * in the standard device identifier attributes.
 * @param ignoreLocationNotEnabled By default, we ensure that the user has chosen
 * use location services.  If they have not we do not collect the location.  This
 * parameter allows you to override this choice which may cause a popup for the user.
 * @param callback The callback block that will be invoked when location collection has
 * completed.
 */
- (void) includeLocationForDeviceData:(BOOL)forDeviceData forDeviceId:(BOOL)forDeviceId ignoreLocationNotEnabled:(BOOL)ignoreLocationNotEnabled withCallback:(ETDFLocationCollectionCompletionBlock)callback;

@end
