//
//  ETDFAttributeTimeZoneOffsetString.h
//  Entrust IdentityGuard Device Fingerprint SDK
//
//  Copyright (c) 2015 Entrust Inc. All rights reserved.
//  Use is subject to the terms of the accompanying license agreement. Entrust Confidential.
//

#import <Foundation/Foundation.h>
#import "ETDFDeviceAttribute.h"

/**
 * Collects the current device time zone offset as a string in the format
 * UTC-4 or UTC+6:30.
 */
@interface ETDFAttributeTimeZoneOffsetString : NSObject<ETDFDeviceAttribute>

- (NSString *) name;
- (id) value;

@end
