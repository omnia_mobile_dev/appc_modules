//
//  ETDFConstants.h
//  Entrust IdentityGuard Device Fingerprint SDK
//
//  Copyright (c) 2015 Entrust Inc. All rights reserved.
//  Use is subject to the terms of the accompanying license agreement. Entrust Confidential.
//

#ifndef EntrustIGDeviceFingerprintSDK_ETDFConstants_h
#define EntrustIGDeviceFingerprintSDK_ETDFConstants_h

#define kETDFVersion @"2.0.0"
#define kETDFFullVersion @"2.0.0.0"

#define kETDFAttributeDeviceModel           @"modelNumber"
#define kETDFAttributeDeviceMachine         @"model"
#define kETDFAttributeScreenHeight          @"screenHeight"
#define kETDFAttributeScreenWidth           @"screenWidth"
#define kETDFAttributeCarrierName           @"carrierName"
#define kETDFAttributeCarrierVOIP           @"carrierVOIP"
#define kETDFAttributeLocale                @"locale"
#define kETDFAttributeMobileISOCountryCode  @"mobileISOCountryCode"
#define kETDFAttributeMobileCountryCode     @"mobileCountryCode"
#define kETDFAttributeMobileNetworkCode     @"mobileNetworkCode"
#define kETDFAttributeDeviceName            @"deviceName"
#define kETDFAttributeDiskSize              @"diskSize"
#define kETDFAttributeDeviceBootTime        @"bootTime"
#define kETDFAttributeScreenScale           @"screenScale"
#define kETDFAttributeOSVersion             @"osVersion"
#define kETDFAttributeTimeZone              @"timezone"
#define kETDFAttributeTimeZoneOffset        @"timezoneOffset"
#define kETDFAttributeTimeZoneOffsetString  @"timezoneOffsetString"
#define kETDFAttributeEmailAccount          @"emailAccount"
#define kETDFAttributeLanguages             @"languages"
#define kETDFAttributeDeviceUnsecure        @"deviceUnsecure"
#define kETDFAttributeDeviceUnsecureReason  @"deviceUnsecureReason"
#define kETDFAttributeDynamicType           @"dynamicType"
#define kETDFAttributeSocialMediaAccounts   @"socialMediaAccounts"
#define kETDFAttributeAccessibilityOptions  @"accessibilityOptions"
#define kETDFAttributeVendorID              @"vendorID"
#define kETDFAttributeInstalledKeyboards    @"keyboards"
#define kETDFAttributeConnectionType        @"connectionType"
#define kETDFAttributeAppVersion            @"appVersion"
#define kETDFAttributeLocation              @"location"
#define kETDFAttributeManufacturer          @"manufacturer"
#define kETDFAttributeIovationBlackbox      @"iovationBlackbox"

#define kETDFAttributePlatform              @"platform"
#define kETDFAttributePlatformValue         @"iOS"

#define kETDFAttributeEntrustSDKVersion     @"version"
#define kETDFAttributes                     @"attributes"

#define kETDFErrorDomain                    @"ETDFErrorDomain"
#define kETDFErrorCodeIovationErrorDescription @"The Iovation (TM) fingerprint module could not be initialized.This may be because the iovation framework was not included in your app. Please refer to the Entrust Datacard RBA Fingerprint SDK Programer's guide for tips on how to resolve this issue."
#define kETDFErrorCodeLocationServicesNotEnabled    1000
#define kETDFErrorCodeLocationServicesDenied        1001
#define kETDFErrorCodeLocationServicesRestricted    1002
#define kETDFErrorCodeIovationBlackboxNotAvailable  1003



#endif
