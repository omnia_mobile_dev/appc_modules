**Fisa Appc Modules**

> Created by [Santiago Plascencia] 

This repository contains [Axway appcelerator] modules for Fisa Mobile Channel.

## Modules

* [Entrust IdentityGuard andorid]
* [Entrust IdentityGuard ios]
* [TiRSA ios]

## Compile and import

First, be sure to have at least .

1. Titanium SDK **7.4.0.GA**.
2. Appcelerator Command-Line Interface **7.0.6**.
3. Node **v8.9.1**
4. Xcode **10.0**
5. Android Studio **3.2**

Build for IOS
```sh
$ cd myAppcModule/ios
$ appc ti build -p ios --build-only
```
Build for Android
```sh
$ cd myAppcModule/android
$ appc ti build -p android --build-only
```

A zip file will be placed at the platform dist folfer, tipically on **android** is myAppcModule/android/dist/myAppcModule-version.zip, on **ios** myAppcModule/ios/myAppcModule-version.zip; uncompress this file and import your plugin in your tiapp.xml file.

[Entrust IdentityGuard andorid]: <https://bitbucket.org/SantiagoPlascencia/appc_modules/src/master/fisaModules/entrustFingerPrint/android>
[Entrust IdentityGuard ios]: <https://bitbucket.org/SantiagoPlascencia/appc_modules/src/master/fisaModules/entrustFingerPrint/ios>
[TiRSA ios]: <https://bitbucket.org/SantiagoPlascencia/appc_modules/src/master/fisaModules/TiRSA>
[Santiago Plascencia]:<https://twitter.com/Blacktiago>
[Axway appcelerator]:<https://www.appcelerator.com/>